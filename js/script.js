/* Place script here */

const loginForm = document.querySelector('.user-form');
loginForm.addEventListener('submit', userLogin);

function userLogin(e) {
  e.preventDefault();

  // Set up Variables for user input and error messages
  const email = document.querySelector('.email').value;
  const password = document.querySelector('.password').value;
  const emailError = document.querySelector('.email-error');
  const passwordError = document.querySelector('.password-error');

  // Reset error messages to hidden
  emailError.style.display = "none";
  passwordError.style.display = "none";

  // Validate user entry
  if (email && password) {
    loginForm.action = 'http://google.co.uk';
    loginForm.submit();
  } else if (!email && !password) {
    emailError.style.display = "block";
    passwordError.style.display = "block";
  } else if (!email) {
    emailError.style.display = "block";
  } else {
    passwordError.style.display = "block";
  }

}
